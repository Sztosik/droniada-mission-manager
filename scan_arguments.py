import json
import config
from area_divider import WaypointsCreator
import config
import os

def get_scan_arguments_json():
    with open("json_files/area.json", "r") as f:
        polygon = json.load(f)
        polygon = polygon["features"][0]["geometry"]["coordinates"][0]
        if polygon[0] == polygon[len(polygon) - 1]:
            polygon = polygon[:-1]

        scan_arguments = {"scan_arguments": {}}
        scan_polygon = []
        altitude = config.ALTITUDE

        for point in polygon:
            lon, lat = point
            scan_point = {"lat": lat, "lon": lon}
            print(f"ScanCoordinate(lat={lat}, lon={lon}),")
            scan_polygon.append(scan_point)

        scan_arguments["scan_arguments"]["polygon"] = scan_polygon
        scan_arguments["scan_arguments"]["altitude"] = altitude

        scan_arguments_json = json.dumps(scan_arguments, indent=2)
        with open("json_files/scan_arguments.json", "w") as f:
            f.write(scan_arguments_json)

if __name__ == "__main__":
    get_scan_arguments_json()
    calc_waypoints = WaypointsCreator(config.ALTITUDE)
    calc_waypoints.run()
    os.system("firefox index.html")
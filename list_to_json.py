import json
import os


def save_json(points_list, file_name):
    POINTS = {"type": "PointsCollection", "points": []}

    for point in points_list:
        POINTS["points"].append([point.lat, point.lon])

    path = os.path.join("./json_files", file_name)
    with open(path, "w") as f:
        json.dump(POINTS, f, indent=2)

from dataclasses import dataclass


@dataclass
class GeoPoint:
    lat: float
    lon: float

import io
import sys

import folium
from folium import plugins
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWidgets import QApplication, QVBoxLayout, QWidget


class AreaSelector(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.interface()

    def interface(self):
        vbox = QVBoxLayout(self)
        self.webEngineView = QWebEngineView()
        self.webEngineView.page().profile().downloadRequested.connect(
            self.handle_downloadRequested
        )

        self.load_map()

        vbox.addWidget(self.webEngineView)
        self.setLayout(vbox)
        self.setGeometry(300, 300, 350, 250)
        self.setWindowTitle("Area Selector GUI")
        self.show()

    def load_map(self):

        # GENERATE MAP
        center = (52.238949, 16.239416)
        m = folium.Map(
            zoom_start=17,
            location=center,
            tiles=None,
        )

        # Google Satellite Layer
        folium.raster_layers.TileLayer(
            tiles="https://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}",
            attr="Google",
            name="Google Satellite",
            overlay=False,
            control=True,
        ).add_to(m)

        # Google Maps Layer
        folium.raster_layers.TileLayer(
            tiles="https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}",
            attr="Google",
            name="Google Maps",
            overlay=False,
            control=True,
        ).add_to(m)

        # MOUSE POSITION
        fmtr = "function(num) {return L.Util.formatNum(num, 8) + ' º ';};"
        plugins.MousePosition(
            position="topright",
            separator=" | ",
            prefix="Mouse:",
            lat_formatter=fmtr,
            lng_formatter=fmtr,
        ).add_to(m)

        # DRAWING THE AREA
        plugins.Draw(
            export=True,
            filename="area.json",
            position="topleft",
            draw_options={
                "marker": False,
                "rectangle": False,
                "circle": False,
                "circlemarker": False,
                "polyline": False,
            },
            edit_options=None,
        ).add_to(m)

        # MEASURE TOOL
        plugins.MeasureControl(
            position="bottomright",
            primary_length_unit="meters",
            secondary_length_unit="miles",
            primary_area_unit="sqmeters",
            secondary_area_unit="acres",
        ).add_to(m)

        m.add_child(folium.LayerControl())
        # save map data to data object
        data = io.BytesIO()
        m.save(data, close_file=False)
        self.webEngineView.setHtml(data.getvalue().decode())

    def handle_downloadRequested(self, item):
        print(item)
        path = "./json_files/area.json"
        print("done")
        if path:
            item.setPath(path)
            item.accept()
            self.close()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setStyleSheet(
        """
        QWidget {
            font-size: 11px;
        }
    """
    )

    area_selector = AreaSelector()
    area_selector.show()

    try:
        sys.exit(app.exec_())
    except SystemExit:
        print("Closing Window...")

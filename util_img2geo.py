import math

import numpy as np


def _get_distance_between_points(x, y, x1, y1):
    return math.sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1))


def _get_fov_from_focal(focal, sensorw, aspectratio):  # mm,mm,mm
    fovy = 2 * np.arctan(sensorw / aspectratio / 2 / focal)  # vertical
    fovx = 2 * np.arctan(sensorw / 2 / focal)  # horizontal
    return fovx, fovy


def _get_pos_of_point(
    posx, posy, resolutionx, resolutiony, fieldx, fieldy
):  # int,int,int,int,m,m
    return ((resolutionx / 2) - posx) / ((resolutionx / 2)) * fieldx, (
        (resolutiony / 2) - posy
    ) / ((resolutiony / 2)) * fieldy


def _get_field_size(height, fovx, fovy):  # m,rad,rad
    return np.tan(fovx / 2) * height, np.tan(fovy / 2) * height


def _get_adjustments(height, roll, pitch):  # m,rad,rad
    return np.tan(roll) * height, np.tan(pitch) * height


def _get_relative_pos(
    focal, sensorw, posx, posy, resolutionx, resolutiony, height
):
    """
    resolution - image resolution,
    roll,pitch,height - meters above ground
    focal-lens focal length (mm)
    sensorw-real width of image sensor (mm)
    posx,posy - pos on image
    """
    aspectratio = resolutionx / resolutiony
    fovx, fovy = _get_fov_from_focal(focal, sensorw, aspectratio)
    fieldx, fieldy = _get_field_size(height, fovx, fovy)
    x, y = _get_pos_of_point(posx, posy, resolutionx, resolutiony, fieldx, fieldy)
    adjx, adjy = _get_adjustments(height, 0, 0)
    x = x + adjx
    y = y + adjy
    return x, y  # VECTOR from current drone position


DRONE = {"north": 0, "east": 0, "height": 30, "roll": 0, "pitch": 0}


class Camera:
    def __init__(self, focal, sensorw, sensorh, resx, resy):
        """
        focal - focal length (35mm),
        sensorw/h - real sensor dimensions
        """
        self.focal = focal
        self.sensorw = sensorw
        self.sensorh = sensorh
        # radial distortion coefficient
        self.rdc = []
        # Pos of distortion ceneter (advanced only (do not apply to lenses which we plan to use))
        self.dcenterx = 0
        self.dcentery = 0
        self.resx = resx
        self.resy = resy
        self.fovx, self.fovy = _get_fov_from_focal(focal, sensorw, sensorw / sensorh)

    def _get_undistored_point(self, x, y):
        infsum = 1
        for i in range(self.rdc):
            infsum = infsum + self.rdc[i] * pow(_get_distance_between_points, i * 2)
        return (x + (self.dcenterx - x) / (infsum)), (
            y + (self.dcentery - y) / (infsum)
        )

    def get_ned_pos(self, x, y, altitude):
        rel_east, rel_north = _get_relative_pos(
            self.focal,
            self.sensorw,
            x,
            y,
            self.resx,
            self.resy,
            altitude,
        )
        north = DRONE["north"] + rel_north
        east = DRONE["east"] + rel_east
        return north, east

    def get_real_area(self, pixels):
        """
        pixels - how many pixels in selected area
        """
        x, y = _get_field_size(DRONE["height"], self.fovx, self.fovy)
        totalarea = x * y
        totalpixels = self.resx * self.resy
        return (pixels / totalpixels) * totalarea

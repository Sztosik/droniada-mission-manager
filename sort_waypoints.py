import json

import list_to_json
from geopy import distance
from points import GeoPoint


def find_min_lat(points):
    min_lat = points[0].lat
    for point in points:
        if point.lat < min_lat:
            min_lat = point.lat
    return min_lat


def find_min_lon(points):
    min_lon = points[0]
    for point in points:
        if point.lon < min_lon.lon:
            min_lon = point
    return min_lon


class SortWaypoints:
    def __init__(self, geojson_path="./json_files/points.json"):
        self.geojson_path = geojson_path
        self.mixed_waypoints = self.load_json()
        self.lat_lon_sorted_layers = list()
        self.sorted = list()

    def load_json(self):
        with open(self.geojson_path, "r") as file:
            geopoints = json.load(file)
        cords = geopoints["points"]

        geopoints = list()
        for item in cords:
            lat, lon = item
            geopoints.append(GeoPoint(lat, lon))
        return geopoints

    def get_lat_layer(self, min_lat):
        """
        looks for points on the same latitude
        """
        lat_layers = list()
        for point in self.mixed_waypoints:
            distance_meters = distance.distance((point.lat, 0), (min_lat, 0)).m
            if distance_meters < 2:
                lat_layers.append(point)
        return lat_layers

    def sort_waypoints(self):
        """
        Sorting waypoints form south to north
        """
        lat_sorted = list()
        lat_lon_sorted = list()

        while len(self.mixed_waypoints) > 0:
            min_lat = find_min_lat(self.mixed_waypoints)
            new_lat_layer = self.get_lat_layer(min_lat)
            lat_sorted.append(new_lat_layer)

            for point in new_lat_layer:
                self.mixed_waypoints.remove(point)

        # west to east layer sort
        for lat_layer in lat_sorted:
            index = lat_sorted.index(lat_layer)
            new_lon_layer = list()

            while len(lat_layer) > 0:
                min_lon = find_min_lon(lat_layer)
                new_lon_layer.append(min_lon)
                lat_layer.remove(min_lon)

            if index % 2 == 0:
                new_lon_layer.reverse()
                lat_lon_sorted.append(new_lon_layer)
            else:
                lat_lon_sorted.append(new_lon_layer)

        self.lat_lon_sorted_layers = lat_lon_sorted

    def save_as_list(self):
        for lat_layer in self.lat_lon_sorted_layers:
            for point in lat_layer:
                self.sorted.append(point)

    def run(self):
        self.sort_waypoints()
        self.save_as_list()
        list_to_json.save_json(self.sorted, "route.json")
        print("The route has been saved to the route.json file")
        return self.sorted


if __name__ == "__main__":
    sort = SortWaypoints()
    sort.run()
